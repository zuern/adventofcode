// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseLine(t *testing.T) {
	line := "vibrant bronze bags contain 3 dim olive bags."
	bagMap := map[string]*Bag{}
	parseLine(line, bagMap)
	require := require.New(t)
	bag := bagMap["vibrant bronze"]
	require.NotNil(bag)
	require.Equal("vibrant bronze", bag.Name)
	require.Equal(len(bag.Contains), 1)
	require.Equal(3, bag.Contains[0].N)
	require.Equal("dim olive", bag.Contains[0].Bag.Name)

	line = "dark orange bags contain 3 bright white bags, 4 muted yellow bags."
	parseLine(line, bagMap)
	bag = bagMap["dark orange"]
	require.NotNil(bag)
	require.Equal("dark orange", bag.Name)
	require.Equal(2, len(bag.Contains))

	bw := bag.Contains[0] // bright white
	my := bag.Contains[1] // muted yellow

	require.Equal(3, bw.N)
	require.Equal("bright white", bw.Bag.Name)

	require.Equal(4, my.N)
	require.Equal("muted yellow", my.Bag.Name)

	line = "faded blue bags contain no other bags."
	parseLine(line, bagMap)
	bag = bagMap["faded blue"]
	require.NotNil(bag)
	require.Equal("faded blue", bag.Name)
	require.Equal(0, len(bag.Contains))
}

func TestPart1(t *testing.T) {
	lines := strings.Split(`light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.
`, "\n")
	n := part1(lines)
	require.Equal(t, 4, n)
}

func TestPart2(t *testing.T) {
	lines := strings.Split(`shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.
`, "\n")
	n := part2(lines)
	require.Equal(t, 126, n)
}
