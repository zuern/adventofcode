// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

// reBag captures the bag description (e.g. "shiny gold") into a capture group,
// ignoring plurality and punctuation:in the string.
var reBag = regexp.MustCompile(`((?:\w+ )+)bags?[,\.]? ?`)

func main() {
	bytes, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(bytes), "\n")
	fmt.Printf("%d bags can eventually contain at least one shiny gold bag.\n", part1(lines))
	fmt.Printf("%d bags are required inside a shiny gold bag.\n", part2(lines))
}

type Bag struct {
	Name     string
	Contains []*Contains
}

type Contains struct {
	N   int // quantity of bag contained.
	Bag *Bag
}

func part1(lines []string) int {
	bags := map[string]*Bag{}
	for _, line := range lines {
		if line == "" {
			continue
		}
		parseLine(line, bags)
	}
	var n int
	const gold = "shiny gold"
	for name, bag := range bags {
		if name == gold {
			continue
		}
		if canContain(gold, bag) {
			n++
		}
	}
	return n
}

func part2(lines []string) int {
	bags := map[string]*Bag{}
	for _, line := range lines {
		if line == "" {
			continue
		}
		parseLine(line, bags)
	}
	return sum(bags["shiny gold"]) - 1
}

// sums the number of bags in the bag.
func sum(b *Bag) int {
	n := 1
	for _, contains := range b.Contains {
		n += contains.N * sum(contains.Bag)
	}
	return n
}

// canContain returns true if b can hold at least one of the named bag.
func canContain(name string, b *Bag) bool {
	if b.Name == name {
		return true
	}
	for _, contains := range b.Contains {
		if canContain(name, contains.Bag) {
			return true
		}
	}
	return false
}

// parseLine takes input like "vibrant bronze bags contain 3 dim olive bags."
// and creates a graph edge [vibrant bronze] -- contains 3 --> [dim olive],
// creating the nodes [vibrant bronze] and [dim olive] as needed.
func parseLine(line string, bagMap map[string]*Bag) {
	parts := strings.Split(line, "contain")
	getBagName := func(s string) string { return strings.TrimSpace(reBag.FindStringSubmatch(s)[1]) }
	bagName := getBagName(parts[0])
	b, have := bagMap[bagName]
	if !have {
		b = &Bag{Name: bagName}
	}
	if strings.Contains(line, "no other bags") {
		bagMap[b.Name] = b
		return
	}
	for _, desc := range strings.Split(parts[1], ",") {
		subBagDesc := getBagName(desc) // something like: "3 dotted blue"
		splitAt := strings.Index(subBagDesc, " ")
		subBagName := subBagDesc[splitAt+1:]
		subBag, have := bagMap[subBagName]
		if !have {
			subBag = &Bag{Name: subBagName}
			bagMap[subBagName] = subBag
		}
		n, _ := strconv.Atoi(subBagDesc[:splitAt])
		b.Contains = append(b.Contains, &Contains{N: n, Bag: subBag})
	}
	bagMap[b.Name] = b
}
