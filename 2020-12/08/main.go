// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	bytes, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}
	instructions := strings.Split(string(bytes), "\n")
	if instructions[len(instructions)-1] == "" {
		instructions = instructions[:len(instructions)-1] // strip trailing newline
	}
	acc, _ := part1(instructions)
	fmt.Printf("Part 1: Accumulator had value %d just before infinite loop.\n", acc)
	acc = part2(instructions)
	fmt.Printf("Part 2: Accumulator had value %d after program was fixed.\n", acc)
}

type processor struct {
	acc          int // accumulator
	pc           int // program counter
	instructions []string
}

// execute the instruction at the program counter. Returns the value of the
// accumulator (acc) and program counter (pc) after execution.
func (p *processor) execute() (acc, pc int, terminated bool) {
	op := p.instructions[p.pc][:3]
	arg, _ := strconv.Atoi(p.instructions[p.pc][4:])
	switch op {
	case "acc":
		p.acc += arg
		p.pc++
	case "nop":
		p.pc++
	case "jmp":
		p.pc += arg
	}
	if p.pc == len(p.instructions) {
		return p.acc, p.pc, true
	}
	return p.acc, p.pc, false
}

// run the program until it terminates or an infinite loop is detected. If the
// program terminates err is nil.
func (p *processor) run() (acc, pc int, err error) {
	var alreadyExecuted = make([]bool, len(p.instructions))
	var terminated bool
	for {
		if acc, pc, terminated = p.execute(); terminated {
			break
		} else if alreadyExecuted[pc] {
			err = fmt.Errorf("infinite loop in program detected")
			break
		}
		alreadyExecuted[pc] = true
	}
	return
}

// part1 finds the value of the accumulator before an instruction is executed
// for the second time (right before the infinite loop starts).
func part1(instructions []string) (acc, pc int) {
	proc := &processor{instructions: instructions}
	acc, pc, _ = proc.run()
	return
}

// part2 fixes the instructions by flipping either a jmp->nop or nop->jmp and
// returns the value of the accumulator after the program terminates. It panics
// if the program cannot be fixed.
func part2(instructions []string) (acc int) {
	// Build a list of instructions executed.
	var alreadyExecuted = make([]bool, len(instructions))
	var pc int
	proc := &processor{instructions: instructions}
	for {
		acc, pc, _ = proc.execute()
		if alreadyExecuted[pc] {
			break
		}
		alreadyExecuted[pc] = true
	}
	var err error
	for i, ins := range instructions {
		op := ins[:3]
		switch op {
		case "jmp":
			op = "nop"
		case "nop":
			op = "jmp"
		default:
			continue
		}
		opOrig := instructions[i]
		instructions[i] = op + " " + instructions[i][4:]
		proc.acc = 0
		proc.pc = 0
		if acc, _, err = proc.run(); err == nil {
			break
		}
		instructions[i] = opOrig
	}
	if err != nil {
		panic("Could not fix the program")
	}
	return acc
}
