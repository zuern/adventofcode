// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPart1(t *testing.T) {
	lines := strings.Split(
		`nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`, "\n")
	acc, pc := part1(lines)
	require.Equal(t, 5, acc)
	require.Equal(t, 1, pc)
}

func TestPart2(t *testing.T) {
	lines := strings.Split(
		`nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`, "\n")
	acc := part2(lines)
	require.Equal(t, 8, acc)
}
