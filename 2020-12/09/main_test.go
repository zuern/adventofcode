// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var nums = []int{
	35,
	20,
	15,
	25,
	47,
	40,
	62,
	55,
	65,
	95,
	102,
	117,
	150,
	182,
	127,
	219,
	299,
	277,
	309,
	576,
}

func TestPart1(t *testing.T) {
	n := part1(5, nums)
	require.Equal(t, 127, n)
}

func TestPart2(t *testing.T) {
	n := part2(part1(5, nums), nums)
	require.Equal(t, 62, n)
}
