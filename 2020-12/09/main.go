// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

func main() {
	bytes, _ := ioutil.ReadFile("input")
	nums := []int{}
	for _, line := range strings.Split(string(bytes), "\n") {
		n, _ := strconv.Atoi(line)
		nums = append(nums, n)
	}
	n := part1(25, nums)
	fmt.Printf("Part 1: n=%d\n", n)
	fmt.Printf("Part 2: n=%d\n", part2(n, nums))
}

// part1 returns the first invalid number.
func part1(lenPreamble int, nums []int) (n int) {
	for i := lenPreamble; i < len(nums); i++ {
		n = nums[i]
		var valid bool
	CHECKVALID:
		for j, x := range nums[i-lenPreamble : i] { // preamble
			for k, y := range nums[i-lenPreamble : i] {
				if j == k {
					// the operands in the sum are distinct numbers in the preamble.
					continue
				}
				if x+y == n {
					valid = true
					break CHECKVALID
				}
			}
		}
		if !valid {
			break
		}
	}
	return n
}

// part2 finds a contiguous set of at least two numbers that sum to the result
// of n.
func part2(n int, nums []int) int {
	var i, end int
FIND:
	for i = range nums {
		end = i + 2
		for end < len(nums) {
			if sum(nums[i:end]) == n {
				break FIND
			}
			end++
		}
	}
	sort.Ints(nums[i:end])
	return nums[i] + nums[end-1]
}

// sum returns the sum of ns.
func sum(ns []int) int {
	if len(ns) == 1 {
		return ns[0]
	}
	return ns[0] + sum(ns[1:])
}
