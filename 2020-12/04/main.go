// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	bytes, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(bytes), "\n")
	fmt.Printf("Part 1: There are %d valid passports.\n", part1(lines))
	fmt.Printf("Part 2: There are %d valid passports.\n", part2(lines))
}

func parse(lines []string) []map[string]string {
	var passports []map[string]string
	pp := map[string]string{}
	for _, line := range lines {
		if line == "" {
			passports = append(passports, pp)
			pp = map[string]string{}
			continue
		}
		segs := strings.Split(line, " ")
		for _, seg := range segs {
			kv := strings.Split(seg, ":")
			pp[kv[0]] = kv[1]
		}
	}
	return passports
}

// count number of passports that have 7 fields, not including "cid".
func part1(lines []string) int {
	passports := parse(lines)
	var n int
	for _, p := range passports {
		if _, have := p["cid"]; have {
			delete(p, "cid")
		}
		if len(p) == 7 {
			n++
		}
	}
	return n
}

// count number of valid passports. ignore cid field, other fields have
// validation rules.
func part2(lines []string) int {
	passports := parse(lines)

	var n int
LOOP:
	for _, p := range passports {
		// All fields present ignoring cid
		if _, have := p["cid"]; have {
			delete(p, "cid")
		}
		if len(p) < 7 {
			continue
		}
		valid := true
		for k, v := range p {
			switch k {
			case "byr":
				valid = intRange(v, 1920, 2002)
			case "iyr":
				valid = intRange(v, 2010, 2020)
			case "eyr":
				valid = intRange(v, 2020, 2030)
			case "hgt":
				valid = heightValid(v)
			case "hcl":
				valid = hairColorValid(v)
			case "ecl":
				valid = eyeColorValid(v)
			case "pid":
				valid = passportIDValid(v)
			}
			if !valid {
				continue LOOP
			}
		}
		n++
	}
	return n
}

/*
	VALIDATION FUNCTIONS FOR PART 2 BELOW:
*/

// s in a number in [min,max]
func intRange(s string, min, max int) bool {
	i, err := strconv.Atoi(s)
	if err != nil {
		return false
	}
	return i >= min && i <= max
}

// heightValid if a number followed by either cm or in:
//   - If cm, the number must be at least 150 and at most 193.
//   - If inches, the number must be at least 59 and at most 76.
func heightValid(s string) bool {
	if i := strings.Index(s, "cm"); i > 0 {
		return intRange(s[0:i], 150, 193)
	} else if i = strings.Index(s, "in"); i > 0 {
		return intRange(s[0:i], 59, 76)
	}
	return false // bad/missing unit.
}

// hairColorValid if a "#" followed by exactly six characters 0-9 or a-f.
func hairColorValid(s string) bool {
	match, _ := regexp.Match(`^#[0-9a-f]{6}$`, []byte(s))
	return match
}

// eyeColorValid if exactly one of: amb blu brn gry grn hzl oth.
func eyeColorValid(s string) bool {
	for _, allowed := range []string{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"} {
		if s == allowed {
			return true
		}
	}
	return false
}

// passportIDValid if a nine-digit number, including leading zeroes.
func passportIDValid(s string) bool {
	match, _ := regexp.Match(`^[0-9]{9}$`, []byte(s))
	return match
}
