// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

/*
	There is a bug somewhere in my validation rules...
*/

func TestPart1(t *testing.T) {
	lines := []string{
		"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm",
		"",
		"iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929",
		"",
		"hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm",
		"",
		"hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in",
	}
	n := part1(lines)
	require.Equal(t, 2, n, "There should be 2 valid passports")
}

func TestPart2(t *testing.T) {
	valid := strings.Split(
		`pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980 hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989 iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785 hgt:164cm byr:2001 iyr:2015 cid:88 pid:545766238 ecl:hzl eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
`, "\n")

	invalid := strings.Split(
		`eyr:1972 cid:100 hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019 hcl:#602927 eyr:1967 hgt:170cm ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012 ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz eyr:2038 hcl:74454a iyr:2023 pid:3556412378 byr:2007
`, "\n")

	require.Equal(t, 4, part2(valid), "valid are counted correctly")
	require.Equal(t, 0, part2(invalid), "invalid are counted correctly")
}

func TestIntRange(t *testing.T) {
	// [input, min, max, valid]
	inputs := [][]interface{}{
		{"2002", 2002, 2003, true},
		{"2003", 2002, 2003, true},
		{"2001", 2002, 2003, false},
		{"2004", 2002, 2003, false},
		{"abc", 0, 0, false},
	}
	for _, input := range inputs {
		str := input[0].(string)
		min := input[1].(int)
		max := input[2].(int)
		valid := input[3].(bool)
		require.Equal(t, valid, intRange(str, min, max), "%s should return %v", str, valid)
	}
}

func TestHeightValid(t *testing.T) {
	inputs := map[string]bool{
		"60in":  true,
		"190cm": true,
		"190in": false,
		"190":   false,
		"149cm": false,
		"150cm": true,
		"193cm": true,
		"194cm": false,
		"58in":  false,
		"59in":  true,
		"76in":  true,
		"77in":  false,
	}
	for s, valid := range inputs {
		require.Equal(t, valid, heightValid(s), "%s should return %v", s, valid)
	}
}

func TestHairColor(t *testing.T) {
	inputs := map[string]bool{
		"#123abc": true,
		"#123abz": false,
		"123abc":  false,
		"#abcdef": true,
		"#123456": true,
		"#7890aa": true,
		// Found it!
		// These two cases below turned out to highlight the bug I was hunting for.
		// My regex was matching a substring, I had to add ^ and $ to make it be an
		// exact match regex.
		"#1234566": false,
		"#1234567": false,
	}
	for s, valid := range inputs {
		require.Equal(t, valid, hairColorValid(s), "%s should return %v", s, valid)
	}
}
