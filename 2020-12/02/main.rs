use std::fs;

fn main() {
    let contents = fs::read_to_string("input").unwrap();
    part1(&contents);
    part2(&contents);
}

// parse takes a string of form "2-8 t: abcdefg" and returns a tuple of (min, max, char, password).
fn parse(s: &str) -> (usize, usize, char, String) {
    // give me ALL the allocations (forgive me, I'm learning).
    let parts = s.split(": ").collect::<Vec<_>>();

    let letter: char = parts[0].split(" ").collect::<Vec<_>>()[1]
        .chars()
        .nth(0)
        .unwrap();

    let policy = parts[0].split(" ").collect::<Vec<_>>()[0];
    let min: usize = policy.split("-").collect::<Vec<_>>()[0].parse().unwrap();
    let max: usize = policy.split("-").collect::<Vec<_>>()[1].parse().unwrap();

    let password = parts[1];
    return (min, max, letter, password.to_string());
}

fn part1(s: &str) {
    let mut valid: i32 = 0;
    for line in s.lines() {
        let (min, max, letter, password) = parse(line);
        let mut n: usize = 0;
        for c in password.chars() {
            if c == letter {
                n += 1;
            }
        }
        if n >= min && n <= max {
            valid += 1;
        }
    }
    println!("Part 1: Num valid passwords: {}", valid);
}

fn part2(s: &str) {
    let mut valid: i32 = 0;
    for line in s.lines() {
        let (mut i, mut j, letter, password) = parse(line);
        i -= 1; // account for z-index.
        j -= 1;
        let pwc = password.chars().count();
        if pwc > i || pwc > j {
            let mut n: usize = 0;
            if password.chars().nth(i).unwrap() == letter {
                n += 1;
            }
            if password.chars().nth(j).unwrap() == letter {
                n += 1;
            }
            if n == 1 {
                valid += 1;
            }
        }
    }
    println!("Part 2: Num valid passwords: {}", valid)
}
