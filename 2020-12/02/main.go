// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	bytes, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(bytes), "\n")
	part1(lines)
	part2(lines)
}

func parse(s string) (min int, max int, letter rune, password string) {
	policy := strings.Split(s, ": ")[0]
	var err error
	if min, err = strconv.Atoi(strings.Split(strings.Split(policy, " ")[0], "-")[0]); err != nil {
		panic(err)
	}
	if max, err = strconv.Atoi(strings.Split(strings.Split(policy, " ")[0], "-")[1]); err != nil {
		panic(err)
	}
	letter = rune(strings.Split(policy, " ")[1][0])
	password = strings.Split(s, ": ")[1]
	return
}

func part1(lines []string) {
	var numValid int
	for _, line := range lines {
		if line == "" {
			continue
		}
		min, max, letter, password := parse(line)
		instances := 0
		for _, char := range password {
			if char == letter {
				instances++
			}
		}
		if instances >= min && instances <= max {
			numValid++
		}
	}
	fmt.Printf("Part 1: Num valid passwords: %d\n", numValid)
}

func part2(lines []string) {
	var numValid int
	for _, line := range lines {
		if line == "" {
			continue
		}
		i, j, letter, password := parse(line)
		i-- // Adjust for 1-indexing.
		j--
		if len(password) > j && len(password) > i {
			n := 0
			if rune(password[i]) == letter {
				n++
			}
			if rune(password[j]) == letter {
				n++
			}
			if n == 1 {
				numValid++
			}
		}
	}
	fmt.Printf("Part 2: Num valid passwords: %d\n", numValid)
}
