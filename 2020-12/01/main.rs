use std::fs;

fn main() {
    let contents = fs::read_to_string("input").unwrap();
    let mut nums: Vec<i32> = Vec::new();
    for s in contents.lines() {
        nums.push(s.parse::<i32>().unwrap());
    }
    println!("Part 1");
    part1(&nums);
    println!("Part 2");
    part2(&nums);
}

fn part1(nums: &Vec<i32>) {
    for i in 0..nums.len() {
        let x = nums[i];
        for j in i..nums.len() {
            let y = nums[j];
            if x + y == 2020 {
                println!("Answer: {}*{} = {}", x, y, x * y);
                return;
            }
        }
    }
}

fn part2(nums: &Vec<i32>) {
    for i in 0..nums.len() {
        let x = nums[i];
        for j in i..nums.len() {
            let y = nums[j];
            for k in j..nums.len() {
                let z = nums[k];
                if x + y + z == 2020 {
                    println!("Answer: {}*{}*{} = {}", x, y, z, x * y * z);
                    return;
                }
            }
        }
    }
}
