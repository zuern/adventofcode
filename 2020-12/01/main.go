// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	bytes, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(bytes), "\n")
	var nums []int
	for i, line := range lines {
		if line == "" {
			continue
		}
		n, err := strconv.Atoi(line)
		if err != nil {
			err = fmt.Errorf("line %d: %w", i, err)
			log.Fatal(err)
		}
		nums = append(nums, n)
	}

	fmt.Println("Part 1")
	part1(&nums)

	fmt.Println("Part 2")
	part2(&nums)
}

func part1(nums *[]int) {
	var x, y int
	for i := 0; i < len(*nums); i++ {
		x = (*nums)[i]
		for j := i; j < len(*nums); j++ {
			y = (*nums)[j]
			if x+y == 2020 {
				fmt.Printf("Answer: %d*%d = %d\n", x, y, x*y)
				return
			}
		}
	}
}

func part2(nums *[]int) {
	var x, y, z int
	for i := 0; i < len(*nums); i++ {
		x = (*nums)[i]
		for j := i; j < len(*nums); j++ {
			y = (*nums)[j]
			for k := j; k < len(*nums); k++ {
				z = (*nums)[k]
				if x+y+z == 2020 {
					fmt.Printf("Answer: %d*%d*%d = %d\n", x, y, z, x*y*z)
					return
				}
			}
		}
	}
}
