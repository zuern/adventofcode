// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	bytes, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(bytes), "\n")
	fmt.Printf("Part 1: sum of counts of questions is %d.\n", part1(lines))
	fmt.Printf("Part 1: sum of counts of questions everyone in group answered yes to is %d.\n", part2(lines))
}

// part1 count sum of questions answered yes to across all groups.
func part1(lines []string) int {
	var n int
	questions := map[rune]struct{}{}
	for _, line := range lines {
		if line == "" {
			n += len(questions)
			questions = map[rune]struct{}{}
		}
		for _, char := range line {
			questions[char] = struct{}{}
		}
	}
	return n
}

func part2(lines []string) int {
	var n, nPpl int
	questions := map[rune]int{} // question -> # of ppl who answered yes in the group.
	for _, line := range lines {
		if line == "" {
			for _, total := range questions {
				if total == nPpl {
					n++
				}
			}
			questions = map[rune]int{}
			nPpl = 0
			continue
		}
		for _, char := range line {
			questions[char]++
		}
		nPpl++
	}
	return n
}
