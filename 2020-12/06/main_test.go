// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPart1(t *testing.T) {
	input := `abc

a
b
c

ab
ac

a
a
a
a

b
`
	sum := part1(strings.Split(input, "\n"))
	require.Equal(t, 11, sum)
}
