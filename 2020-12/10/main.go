// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

func main() {
	bytes, _ := ioutil.ReadFile("input")
	lines := strings.Split(string(bytes), "\n")
	var nums []int
	for _, l := range lines {
		if l == "" {
			continue
		}
		n, _ := strconv.Atoi(l)
		nums = append(nums, n)
	}
	fmt.Printf("Part 1: %d\n", part1(nums))
	fmt.Printf("Part 2: %d\n", part2(nums))
}

func part1(adapters []int) (n int) {
	sort.Ints(adapters)
	// device is greatest adapter + 3 jolts
	adapters = append(adapters, adapters[len(adapters)-1]+3)
	n1, n3 := p1(append([]int{0}, adapters...))
	return n1 * n3
}

// p1 recursively finds distribution of one and three jolt differences in
// between adapters. Requires sorted array with first element having value 0
// and last element to be the "device".
func p1(adapters []int) (n1, n3 int) {
	if len(adapters) == 1 {
		return
	}
	var i int
	for i = 1; i < len(adapters); i++ {
		switch adapters[i] - adapters[0] {
		case 1:
			n1++
		case 3:
			n3++
		}
		s1, s3 := p1(adapters[i:])
		n1 += s1
		n3 += s3
		break
	}
	return
}

// part2 finds number of possible arrangements of adapters such that all
// adapters are used.
func part2(adapters []int) int {
	sort.Ints(adapters)
	nums := make([]int, len(adapters)+1)
	copy(nums[1:], adapters)

	solutions := make([]int, len(nums))
	solutions[0] = 1

	for i, x := range nums {
		for y := 1; y < 4; y++ {
			for j, n := range nums {
				if n == x+y {
					solutions[j] += solutions[i]
					break
				}
			}
		}
	}

	return solutions[len(solutions)-1]
}
