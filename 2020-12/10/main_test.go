// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var set1 = []int{16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4}
var set2 = []int{
	28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19,
	38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3}

func TestPart1(t *testing.T) {
	n := part1(set1)
	require.Equal(t, 35, n)
	n = part1(set2)
	require.Equal(t, 220, n)
}

func TestPart2(t *testing.T) {
	n := part2(set1)
	require.Equal(t, 8, n)
	n = part2(set2)
	require.Equal(t, 19208, n)
}
