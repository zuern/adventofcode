// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	bytes, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(bytes), "\n")
	trees := parse(lines)
	part1(trees)
	part2(trees)
}

// parse into grid of trees, true if a tree is in that spot.
func parse(lines []string) (trees [][]bool) {
	for _, line := range lines {
		if line == "" {
			break
		}
		row := make([]bool, len(line))
		for i, x := range line {
			if x == '#' {
				row[i] = true
			}
		}
		trees = append(trees, row)
	}
	return trees
}

// Count number of trees in spots going down 1 over 3 each time.
func part1(trees [][]bool) {
	var n int
	var col int
	for row := 0; row < len(trees); row++ {
		if trees[row][col] {
			n++
		}
		col = (col + 3) % len(trees[0])
	}
	fmt.Printf("Part 1: %d trees encountered.\n", n)
}

// Count number of trees in spots across multiple paths, multiply counts
// together.
func part2(trees [][]bool) {
	checkSlope := func(right, down int) (n int) {
		var col int
		for row := 0; row < len(trees); row += down {
			if trees[row][col] {
				n++
			}
			col = (col + right) % len(trees[0])
		}
		return n
	}
	patterns := [][2]int{
		{1, 1},
		{3, 1}, // go right 3, down 1
		{5, 1},
		{7, 1},
		{1, 2},
	}
	var totals []int
	for _, pat := range patterns {
		totals = append(totals, checkSlope(pat[0], pat[1]))
	}
	var product int
	for i, t := range totals {
		if i == 0 {
			product = t
			continue
		}
		product *= t
	}
	fmt.Printf("Part 2: %d is the product of the trees encountered.\n", product)
}
