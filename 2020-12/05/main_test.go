// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDecode(t *testing.T) {
	inputs := map[string][]int{
		"FBFBBFFRLR": {44, 5},
		"BFFFBBFRRR": {70, 7},
		"FFFBBBFRRR": {14, 7},
		"BBFFBBFRLL": {102, 4},
	}
	for boardingPass, expect := range inputs {
		row, col := decode(boardingPass)
		require.Equal(t, expect, []int{row, col}, "%s should return %v but didn't.", boardingPass, expect)
	}
}
