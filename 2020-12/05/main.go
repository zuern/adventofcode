// Copyright 2020 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strings"
)

func main() {
	bytes, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(string(bytes), "\n")
	fmt.Printf("Part 1: The highest ID is %d\n", part1(lines))
	fmt.Printf("Part 2: My seat ID is %d\n", part2(lines))
}

// decode a boarding pass (BFFFBBFRRR) to a [row,col]. This function panics if
// an exact seat is not found.
func decode(bp string) (row, col int) {
	// 128 rows, 8 columns (0-indexed).
	rMin, rMax, cMin, cMax := 0, 127, 0, 7
	for _, char := range bp {
		switch char {
		case 'F':
			rMax = rMin + ((rMax - rMin) / 2)
		case 'B':
			rMin = rMin + ((rMax - rMin) / 2) + 1
		case 'L':
			cMax = cMin + ((cMax - cMin) / 2)
		case 'R':
			cMin = cMin + ((cMax - cMin) / 2) + 1
		}
	}
	if rMax != rMin {
		panic(fmt.Sprintf("row calc failed: boarding pass %q max: %d min %d", bp, rMax, rMin))
	}
	if cMax != cMin {
		panic(fmt.Sprintf("col calc failed: boarding pass %q max: %d min %d", bp, cMax, cMin))
	}
	return rMax, cMax
}

// part1 finds the highest ID boarding pass in the list.
func part1(lines []string) int {
	var highestID int
	for _, boardingPass := range lines {
		if len(boardingPass) == 0 {
			continue
		}
		row, column := decode(boardingPass)
		if id := (row * 8) + column; id > highestID {
			highestID = id
		}
	}
	return highestID
}

// find missing id in the list (gap between two ids). Returns -1 if no gap
// found.
func part2(lines []string) int {
	ids := []int{}
	for _, bp := range lines {
		if len(bp) == 0 {
			continue
		}
		row, col := decode(bp)
		id := (row * 8) + col
		ids = append(ids, id)
	}
	sort.Ints(ids)
	last := ids[0]
	for i, id := range ids {
		if i == 0 {
			continue
		}
		if id-last > 1 {
			return id - 1
		}
		last = id
	}
	return -1
}
