Advent of Code
----------------

This repo holds my solutions to the [Advent of Code](https://adventofcode.com/)
puzzles.

## 2020
This is my first year and I've decided to try and solve them in Rust. I've never
programmed in Rust so this should be a fun challenge!
